import models.Date;

public class App {
    public static void main(String[] args) throws Exception {
        Date date1 = new Date(26,05,2023);
        Date date2 = new Date(30,04,2023);
        
        System.out.println(date1.toString());
        System.out.println(date2.toString());
    }
}
